mkdir ~/svg-script-folder
echo LISTE PACMAN > ~/svg-script-folder/svg-script.txt
comm -12 <(grep -Poe '\[ALPM\] installed \K\S*' /var/log/pacman.log | sort | uniq) <(pacman -Qeqn | sort) >> ~/svg-script-folder/svg-script.txt
echo LISTE FLATPAK >> ~/svg-script-folder/svg-script.txt
flatpak list >> ~/svg-script-folder/svg-script.txt
echo LISTE SNAP >> ~/svg-script-folder/svg-script.txt
snap list >> ~/svg-script-folder/svg-script.txt
echo LISTE YAY >> ~/svg-script-folder/svg-script.txt
comm -12 <(grep -Poe '\[ALPM\] installed \K\S*' /var/log/pacman.log | sort | uniq) <(pacman -Qeqm | sort) >> ~/svg-script-folder/svg-script.txt
cp .config/hypr/hyprland.conf ~/svg-script-folder/
