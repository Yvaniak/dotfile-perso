#!/bin/bash

x=$(brightnessctl g)
y=$(brightnessctl max)

diff=$(echo "scale=4; $x / $y" | bc)

echo $diff
echo $(echo "$diff < 0.1" | bc)

change=5

if [[ $(echo "$diff < 0.1" | bc) ]]; then
    change=1
fi
echo $change
if [[ $1 == "+" ]]; then
    if [[ -e ~/scripts/state && $(brightnessctl g) == 0 ]]; then
        rm ~/scripts/state
    fi
    $(brightnessctl s +$change%)
else
    $(brightnessctl s $change%-)
fi