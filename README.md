# Dotfile Perso Yvaniak

## Description
Little dotfile to put my config files

## Installation
`git clone https://gitlab.com/Yvaniak/dotfile-perso.git/`



## Authors and acknowledgment
Thanks to [JaKooLit](https://github.com/JaKooLit/), I only put some config files (mainly for keybinds) on top of is [Hyprland-v3](https://github.com/JaKooLit/Hyprland-v3)
